<html xmlns:f="http://typo3.org/ns/TYPO3/CMS/Fluid/ViewHelpers"
  xmlns:med="http://typo3.org/ns/MED/Medgooglemaps/ViewHelpers"
  xmlns:razor="http://typo3.org/ns/RZ/Razor/ViewHelpers"
  xmlns:v="http://typo3.org/ns/FluidTYPO3/Vhs/ViewHelpers"
  data-namespace-typo3-fluid="true">

var map{uid};
var marker = [];
var autoOpen = [];

function initialize{uid}() {
  var myLatlng{uid} = new google.maps.LatLng({latitude},{longitude});

  var myOptions{uid} = {
      scrollwheel: <f:format.raw />{scrollWheel},
      draggable: {draggable},
      zoom: {zoom},
      <f:format.raw>{styleOutput}</f:format.raw>
      center: myLatlng{uid},
      mapTypeId: google.maps.{mapTypeOutput}
      {controlsConfig}
  };

  map{uid} = new google.maps.Map(document.getElementById("map_canvas_{uid}"), myOptions{uid});

  <f:if condition="{autocalcmapcenter}">
    var bounds = new google.maps.LatLngBounds();
  </f:if>

  <f:if condition="{markers}">
    <f:for each="{markers}" as="marker" iteration="i">
      <v:variable.set name="infotext" value="" />
      <f:if condition="{marker.infotext}">
        <v:variable.set name="infotext" value='<div class="medgooglemaps_content"><med:replace content="<f:format.html>{marker.infotext}</f:format.html>" /></div>' />
      </f:if>

      var contentString{i.cycle} =
        ['<f:format.raw>{infotext}</f:format.raw>'<f:if condition="{marker.navigation}">,</f:if>
        <f:if condition="{marker.navigation}"><med:array obj="{navigationArray}" prop="{i.cycle}" /></f:if>
        ].join("\n");
    </f:for>

    var locations = [
      <f:for each="{markers}" as="marker" iteration="i">
        <v:variable.set name="markercustom" value="" />
        <f:if condition="{marker.markercustom}">
          <v:variable.set name="markercustom" value="{f:uri.image(src: '{marker.markercustom.uid}', treatIdAsReference: 1)}" />
        </f:if>

        [contentString{i.cycle}, {marker.latitude}, {marker.longitude}, {f:if(condition: '{marker.infowindowautoopen}', then: '1', else: '0')}, '{marker.markercolor}', '{f:if(condition: marker.markercustom, then: '{markercustom}')}'],
      </f:for>
    ];

    var i;

    for (i = 0; i < locations.length; i++) {
      <f:if condition="{autocalcmapcenter}">
        bounds.extend(new google.maps.LatLng(locations[i][1], locations[i][2]));
      </f:if>

      <f:for each="{markers}" as="marker" iteration="i">
        var markerSize = new google.maps.Size(26, 39);

        if(locations[i][4]) {
          var markerIcon = "/typo3conf/ext/medgooglemaps/Resources/Public/Icons/marker/"+locations[i][4]+".svg";
        }
        else {
          var markerIcon = "";
        }

        if(locations[i][5]) {
          var markerIcon = locations[i][5];
          var markerSize = new google.maps.Size(64, 64);
        }

        marker[i] = new google.maps.Marker({
          position: new google.maps.LatLng(locations[i][1], locations[i][2]),
          map: map{uid},
          icon: {
            url: markerIcon,
            scaledSize: markerSize
          }
        });

        if(locations[i][0]) {
          marker[i].medInfoWindow = new google.maps.InfoWindow({
            content: locations[i][0],
            disableAutoPan: true
          });

          google.maps.event.addListener(marker[i], "click", function() {
            // Close previously opened infowindows
            for (i = 0; i < locations.length; i++) {
              if(marker[i].medInfoWindow) {
                marker[i].medInfoWindow.close();
              }
            }

            this.medInfoWindow.disableAutoPan = false;
            this.medInfoWindow.open(map<f:format.raw />{uid},this);
          });

          if(locations[i][3] == 1) {
            autoOpen.push(i);
          }
        }
      </f:for>
    }
  </f:if>

  <f:if condition="{autocalcmapcenter}">
    map{uid}.fitBounds(bounds);
    map{uid}.panToBounds(bounds);
    var boundsChangedListener = google.maps.event.addListenerOnce(map{uid}, 'bounds_changed', function(event) {
      if(this.getZoom()) {
        this.setZoom(myOptions<f:format.raw />{uid}.zoom);
      }
      google.maps.event.removeListener(boundsChangedListener);
    });
  </f:if>

  google.maps.event.addListenerOnce(map{uid}, "idle", function() {
    if(autoOpen.length > 0) {
      for (var i = 0; i < autoOpen.length; i++) {
        marker[autoOpen[i]].medInfoWindow.open(map{uid}, marker[autoOpen[i]]);
      }
    }
  });
}

<f:if condition="{centerOnResize}">
  google.maps.event.addDomListener(window, "resize", function() {
    var center = map{uid}.getCenter();
    google.maps.event.trigger(map{uid}, "resize");
    map{uid}.setCenter(center);
  });
</f:if>

google.maps.event.addDomListener(window, "load", initialize{uid});
