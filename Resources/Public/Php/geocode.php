<?php

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

$apiKey = $_POST['apiKey'];

$address = $_POST['address'];
$zip = $_POST['zip'];
$city = $_POST['city'];
$country = $_POST['country'];

// Build string
$addressFinal = urlencode($address . ', ' . $zip . ' ' . $city . ', ' . $country);

$url = 'https://maps.google.com/maps/api/geocode/xml?address=' . $addressFinal . '&key=' . $apiKey;
$output = file_get_contents_curl($url);

// Load XML
$xml = simplexml_load_string($output);

if ($xml->status == 'OK') {
    // Latitude/Longitude
    $lat = (string) $xml->result->geometry->location->lat;
    $lng = (string) $xml->result->geometry->location->lng;

    $arr = [
        'lat' => $lat,
        'lng' => $lng,
    ];

    echo json_encode($arr);
}

function file_get_contents_curl($url)
{
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Set curl to return the data instead of printing it to the browser.
    curl_setopt($ch, CURLOPT_URL, $url);

    $data = curl_exec($ch);
    curl_close($ch);

    return $data;
}
