<?php

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

if (ExtensionManagementUtility::isLoaded('razor')) {
    $rteConfig = 'razor';
} else {
    $rteConfig = 'default';
}

$GLOBALS['TCA']['tx_medgooglemaps_domain_model_marker'] = [
    'ctrl' => [
        'title' => 'LLL:EXT:medgooglemaps/Resources/Private/Language/locallang_db.xlf:tx_medgooglemaps_domain_model_marker',
        'label' => 'address',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'dividers2tabs' => true,
        'hideTable' => true,
        'versioningWS' => false,
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'sortby' => 'sorting',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'address,',
        'iconfile' => 'EXT:medgooglemaps/Resources/Public/Icons/tx_medgooglemaps_domain_model_marker.svg',
    ],
    'interface' => [
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, address, zip, city, country, geocode, latitude, longitude, infoText, infowindowautoopen, navigation, markercolor, markercustom',
    ],
    'types' => [
        '1' => [
            'showitem' => 'l10n_parent, l10n_diffsource,
			--palette--;LLL:EXT:medgooglemaps/Resources/Private/Language/locallang_db.xlf:tx_medgooglemaps_domain_model_marker.paletteAddress;paletteAddress,

			geocode,--palette--;;paletteLatLng,

			--div--;LLL:EXT:medgooglemaps/Resources/Private/Language/locallang_db.xlf:tx_medgooglemaps_domain_model_marker.tabInfowindow,
			 	infoText, infowindowautoopen, navigation,

			--div--;LLL:EXT:medgooglemaps/Resources/Private/Language/locallang_db.xlf:tx_medgooglemaps_domain_model_marker.tabOptions,
				--palette--;LLL:EXT:medgooglemaps/Resources/Private/Language/locallang_db.xlf:tx_medgooglemaps_domain_model_marker.paletteCore;paletteCore,
				--palette--;LLL:EXT:medgooglemaps/Resources/Private/Language/locallang_db.xlf:tx_medgooglemaps_domain_model_marker.paletteMarker;paletteMarker,
			',
        ],
    ],
    'palettes' => [
        'paletteCore' => [
            'showitem' => 'hidden, sys_language_uid',
            'canNotCollapse' => true,
        ],
        'paletteAddress' => [
            'showitem' => 'address, --linebreak--, zip, city, --linebreak--, country',
            'canNotCollapse' => true,
        ],
        'paletteLatLng' => [
            'showitem' => 'latitude, longitude',
            'canNotCollapse' => true,
        ],
        'paletteMarker' => [
            'showitem' => 'markercolor, --linebreak--, markercustom',
            'canNotCollapse' => true,
        ],
    ],
    'columns' => [

        'sys_language_uid' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'sys_language',
                'foreign_table_where' => 'ORDER BY sys_language.title',
                'items' => [
                    [
                        'LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages',
                        -1,
                    ],
                    [
                        'LLL:EXT:lang/locallang_general.xlf:LGL.default_value',
                        0,
                    ],
                ],
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    [
                        '',
                        0,
                    ],
                ],
                'foreign_table' => 'tx_medgooglemaps_domain_model_marker',
                'foreign_table_where' => 'AND tx_medgooglemaps_domain_model_marker.pid=###CURRENT_PID### AND tx_medgooglemaps_domain_model_marker.sys_language_uid IN (-1,0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],

        't3ver_label' => [
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            ],
        ],

        'hidden' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
            'config' => [
                'type' => 'check',
            ],
        ],
        'starttime' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
            'config' => [
                'behaviour' => [
                    'allowLanguageSynchronization' => true
                ],
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 13,
                'eval' => 'datetime',
                'checkbox' => 0,
                'default' => 0,
                'range' => [
                    'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y')),
                ],
            ],
        ],
        'endtime' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
            'config' => [
                'behaviour' => [
                    'allowLanguageSynchronization' => true
                ],
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 13,
                'eval' => 'datetime',
                'checkbox' => 0,
                'default' => 0,
                'range' => [
                    'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y')),
                ],
            ],
        ],

        'address' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:medgooglemaps/Resources/Private/Language/locallang_db.xlf:tx_medgooglemaps_domain_model_marker.address',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim, required',
            ],
        ],

        'zip' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:medgooglemaps/Resources/Private/Language/locallang_db.xlf:tx_medgooglemaps_domain_model_marker.zip',
            'config' => [
                'type' => 'input',
                'size' => 5,
                'eval' => 'trim, required',
            ],
        ],

        'city' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:medgooglemaps/Resources/Private/Language/locallang_db.xlf:tx_medgooglemaps_domain_model_marker.city',
            'config' => [
                'type' => 'input',
                'size' => 19,
                'eval' => 'trim, required',
            ],
        ],

        'country' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:medgooglemaps/Resources/Private/Language/locallang_db.xlf:tx_medgooglemaps_domain_model_marker.country',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required',
            ],
        ],

        'geocode' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:medgooglemaps/Resources/Private/Language/locallang_db.xlf:tx_medgooglemaps_domain_model_marker.geocode',
            'config' => [
                'type' => 'user',
                'userFunc' => 'MED\Medgooglemaps\Utility\Geocode->geocodeSectionIrre',
            ],
        ],

        'latitude' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:medgooglemaps/Resources/Private/Language/locallang_db.xlf:tx_medgooglemaps_domain_model_marker.latitude',
            'config' => [
                'type' => 'input',
                'size' => 8,
                'eval' => 'trim',
            ],
        ],

        'longitude' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:medgooglemaps/Resources/Private/Language/locallang_db.xlf:tx_medgooglemaps_domain_model_marker.longitude',
            'config' => [
                'type' => 'input',
                'size' => 8,
                'eval' => 'trim',
            ],
        ],

        'infoText' => [
            'exclude' => 1,
            'l10n_mode' => 'prefixLangTitle',
            'label' => 'LLL:EXT:medgooglemaps/Resources/Private/Language/locallang_db.xlf:tx_medgooglemaps_domain_model_marker.infoText',
            'config' => [
                'type' => 'text',
                'enableRichtext' => true,
                'richtextConfiguration' => $rteConfig,
                'fieldControl' => [
                    'fullScreenRichtext' => [
                        'disabled' => false,
                    ],
                ],
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim',
            ],
        ],

        'infowindowautoopen' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:medgooglemaps/Resources/Private/Language/locallang_db.xlf:tx_medgooglemaps_domain_model_marker.infowindowautoopen',
            'config' => [
                'type' => 'check',
            ],
        ],

        'navigation' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:medgooglemaps/Resources/Private/Language/locallang_db.xlf:tx_medgooglemaps_domain_model_marker.navigation',
            'config' => [
                'type' => 'check',
            ],
        ],

        'markercolor' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:medgooglemaps/Resources/Private/Language/locallang_db.xlf:tx_medgooglemaps_domain_model_marker.markercolor',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    [
                        'LLL:EXT:medgooglemaps/Resources/Private/Language/locallang_db.xlf:tx_medgooglemaps_domain_model_marker.markercolor.1',
                        'marker-red',
                    ],
                    [
                        'LLL:EXT:medgooglemaps/Resources/Private/Language/locallang_db.xlf:tx_medgooglemaps_domain_model_marker.markercolor.2',
                        'marker-green',
                    ],
                    [
                        'LLL:EXT:medgooglemaps/Resources/Private/Language/locallang_db.xlf:tx_medgooglemaps_domain_model_marker.markercolor.3',
                        'marker-blue',
                    ],
                    [
                        'LLL:EXT:medgooglemaps/Resources/Private/Language/locallang_db.xlf:tx_medgooglemaps_domain_model_marker.markercolor.4',
                        'marker-yellow',
                    ],
                    [
                        'LLL:EXT:medgooglemaps/Resources/Private/Language/locallang_db.xlf:tx_medgooglemaps_domain_model_marker.markercolor.5',
                        'marker-orange',
                    ],
                    [
                        'LLL:EXT:medgooglemaps/Resources/Private/Language/locallang_db.xlf:tx_medgooglemaps_domain_model_marker.markercolor.6',
                        'marker-black',
                    ],
                    [
                        'LLL:EXT:medgooglemaps/Resources/Private/Language/locallang_db.xlf:tx_medgooglemaps_domain_model_marker.markercolor.7',
                        'marker-white',
                    ],
                ],
                'size' => 1,
                'maxitems' => 1,
                'eval' => '',
            ],
        ],

        // 'markercustom' => [
        //     'exclude' => 1,
        //     'label' => 'LLL:EXT:medgooglemaps/Resources/Private/Language/locallang_db.xlf:tx_medgooglemaps_domain_model_marker.markercustom',
        //     'config' => [
        //         'type' => 'group',
        //         'internal_type' => 'file',
        //         'uploadfolder' => 'uploads/pics',
        //         'size' => 1,
        //         'maxitems' => '1',
        //         'allowed' => 'jpg,jpeg,gif,png,svg',
        //         'disallowed' => '',
        //         'max_size' => 20000,
        //     ],
        // ],

        'markercustom' => [
            'exclude' => true,
            'label' => 'LLL:EXT:medgooglemaps/Resources/Private/Language/locallang_db.xlf:tx_medgooglemaps_domain_model_marker.markercustom',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                'markercustom',
                [
                    'appearance' => [
                        'createNewRelationLinkTitle' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:images.addFileReference',
                    ],
                    'overrideChildTca' => [
                        'types' => [
                            '0' => [
                                'showitem' => '
                                --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                                --palette--;;filePalette',
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => [
                                'showitem' => '
                                --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                                --palette--;;filePalette',
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                                'showitem' => '
                                --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                                --palette--;;filePalette',
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => [
                                'showitem' => '
                                --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                                --palette--;;filePalette',
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => [
                                'showitem' => '
                                --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                                --palette--;;filePalette',
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => [
                                'showitem' => '
                                --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                                --palette--;;filePalette',
                            ],
                        ],
                    ],
                    'maxitems' => 1,
                ],
                'jpg,jpeg,gif,png,svg'
            ),
        ],


    ],
];