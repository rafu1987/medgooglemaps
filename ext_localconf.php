<?php

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function ($extKey) {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'MED.' . $extKey,
            'Maps',
            array(
                'Maps' => 'list',

            ),
            // non-cacheable actions
            array(
                'Maps' => '',

            )
        );

        if (TYPO3_MODE === 'BE') {
            $icons = [
                'ext-medgooglemaps-wizard-icon' => 'ce_wiz.svg',
            ];
            $iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
            foreach ($icons as $identifier => $path) {
                $iconRegistry->registerIcon(
                    $identifier,
                    \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
                    ['source' => 'EXT:medgooglemaps/Resources/Public/Icons/' . $path]
                );
            }
        }

        // wizards
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
            'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    medgooglemaps {
                        iconIdentifier = ext-medgooglemaps-wizard-icon
                        title = LLL:EXT:medgooglemaps/Resources/Private/Language/locallang_be.xlf:medgooglemaps_maps_pluginWizardTitle
                        description = LLL:EXT:medgooglemaps/Resources/Private/Language/locallang_be.xlf:medgooglemaps_maps_pluginWizardDescription
                        tt_content_defValues {
                            CType = list
                            list_type = medgooglemaps_maps
                        }
                    }
                }
                show = *
            }
       }'
        );

    },
    $_EXTKEY
);
