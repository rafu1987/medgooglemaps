<?php

namespace MED\Medgooglemaps\Tests\Unit\Domain\Model;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

/**
 * Test case for class \MED\Medgooglemaps\Domain\Model\Maps.
 *
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 * @author Raphael Zschorsch <rafu1987@gmail.com>
 */
class MapsTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \MED\Medgooglemaps\Domain\Model\Maps
     */
    protected $subject = null;

    protected function setUp()
    {
        $this->subject = new \MED\Medgooglemaps\Domain\Model\Maps();
    }

    protected function tearDown()
    {
        unset($this->subject);
    }

    /**
     * @test
     */
    public function dummyTestToNotLeaveThisFileEmpty()
    {
        $this->markTestIncomplete();
    }
}
