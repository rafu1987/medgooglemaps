<?php
namespace MED\Medgooglemaps\Tests\Unit\Controller;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

/**
 * Test case for class MED\Medgooglemaps\Controller\MapsController.
 *
 * @author Raphael Zschorsch <rafu1987@gmail.com>
 */
class MapsControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{

    /**
     * @var \MED\Medgooglemaps\Controller\MapsController
     */
    protected $subject = null;

    protected function setUp()
    {
        $this->subject = $this->getMock('MED\\Medgooglemaps\\Controller\\MapsController', array(
            'redirect',
            'forward',
            'addFlashMessage',
        ), array(), '', false);
    }

    protected function tearDown()
    {
        unset($this->subject);
    }

    /**
     * @test
     */
    public function listActionFetchesAllMapssFromRepositoryAndAssignsThemToView()
    {

        $allMapss = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array(), array(), '', false);

        $mapsRepository = $this->getMock('MED\\Medgooglemaps\\Domain\\Repository\\MapsRepository', array(
            'findAll',
        ), array(), '', false);
        $mapsRepository->expects($this->once())->method('findAll')->will($this->returnValue($allMapss));
        $this->inject($this->subject, 'mapsRepository', $mapsRepository);

        $view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
        $view->expects($this->once())->method('assign')->with('mapss', $allMapss);
        $this->inject($this->subject, 'view', $view);

        $this->subject->listAction();
    }
}
