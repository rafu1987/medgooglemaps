<?php
namespace MED\Medgooglemaps\Domain\Model;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

/**
 * Marker
 */
class Marker extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * address
     *
     * @var string
     */
    protected $address = '';

    /**
     * zip
     *
     * @var string
     */
    protected $zip = '';

    /**
     * city
     *
     * @var string
     */
    protected $city = '';

    /**
     * country
     *
     * @var string
     */
    protected $country = '';

    /**
     * latitude
     *
     * @var string
     */
    protected $latitude = '';

    /**
     * longitude
     *
     * @var string
     */
    protected $longitude = '';

    /**
     * geocode
     *
     * @var string
     */
    protected $geocode = '';

    /**
     * Infotext
     *
     * @var string
     */
    protected $infotext;

    /**
     * infowindowautoopen
     *
     * @var boolean
     */
    protected $infowindowautoopen = false;

    /**
     * navigation
     *
     * @var boolean
     */
    protected $navigation = false;

    /**
     * markercolor
     *
     * @var string
     */
    protected $markercolor;

    /**
     * markercustom
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     */
    protected $markercustom = null;

    /**
     * Returns the address
     *
     * @return string $address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Sets the address
     *
     * @param string $address
     * @return void
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * Returns the zip
     *
     * @return string $zip
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * Sets the zip
     *
     * @param string $zip
     * @return void
     */
    public function setZip($zip)
    {
        $this->zip = $zip;
    }

    /**
     * Returns the city
     *
     * @return string $city
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Sets the city
     *
     * @param string $city
     * @return void
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * Returns the country
     *
     * @return string $country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Sets the country
     *
     * @param string $country
     * @return void
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * Returns the latitude
     *
     * @return string $latitude
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Sets the latitude
     *
     * @param string $latitude
     * @return void
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    }

    /**
     * Returns the longitude
     *
     * @return string $longitude
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Sets the longitude
     *
     * @param string $longitude
     * @return void
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    }

    /**
     * Returns the geocode
     *
     * @return string $geocode
     */
    public function getGeocode()
    {
        return $this->geocode;
    }

    /**
     * Sets the geocode
     *
     * @param string $geocode
     * @return void
     */
    public function setGeocode($geocode)
    {
        $this->geocode = $geocode;
    }

    /**
     * Returns the infotext
     *
     * @return string $infotext
     */
    public function getInfotext()
    {
        return $this->infotext;
    }

    /**
     * Sets the infotext
     *
     * @param string $infotext
     * @return void
     */
    public function setInfotext($infotext)
    {
        $this->infotext = $infotext;
    }

    /**
     * Returns the infowindowautoopen
     *
     * @return boolean $infowindowautoopen
     */
    public function getInfowindowautoopen()
    {
        return $this->infowindowautoopen;
    }

    /**
     * Sets the infowindowautoopen
     *
     * @param boolean $infowindowautoopen
     * @return void
     */
    public function setInfowindowautoopen($infowindowautoopen)
    {
        $this->infowindowautoopen = $infowindowautoopen;
    }

    /**
     * Returns the navigation
     *
     * @return boolean $navigation
     */
    public function getNavigation()
    {
        return $this->navigation;
    }

    /**
     * Sets the navigation
     *
     * @param boolean $navigation
     * @return void
     */
    public function setNavigation($navigation)
    {
        $this->navigation = $navigation;
    }

    /**
     * @return \integer $markercolor
     */
    public function getMarkercolor()
    {
        return $this->markercolor;
    }

    /**
     * Sets the markercolor
     *
     * @param string $markercolor
     * @return void
     */
    public function setMarkercolor($markercolor)
    {
        $this->markercolor = $markercolor;
    }

    /**
     * Returns the markercustom
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $markercustom
     */
    public function getMarkercustom()
    {
        return $this->markercustom;
    }

    /**
     * Sets the markercustom
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $markercustom
     * @return void
     */
    public function setMarkercustom(\TYPO3\CMS\Extbase\Domain\Model\FileReference $markercustom)
    {
        $this->markercustom = $markercustom;
    }


}