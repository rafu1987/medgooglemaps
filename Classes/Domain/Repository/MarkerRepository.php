<?php
namespace MED\Medgooglemaps\Domain\Repository;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

/**
 * The repository for Markers
 */
class MarkerRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{

    protected $orderings = null;
    protected $respectEnableFields = true;
    protected $respectStoragePage = true;
    protected $limit = null;
    protected $offset = null;

    public function findByFilter($findArray)
    {
        $query = $this->createQuery();

        if (!is_null($this->orderings)) {
            $query->setOrderings($this->orderings);
        }

        if (!is_null($this->limit)) {
            $query->setLimit($this->limit);
        }

        if (!is_null($this->offset)) {
            $query->setOffset($this->offset);
        }

        $query->getQuerySettings()
        //->setRespectEnableFields($this->respectEnableFields)
            ->setRespectStoragePage($this->respectStoragePage);

        if (empty($findArray)) {
            return array();
        }

        foreach ($findArray as $findArrayInner) {

            if (empty($findArrayInner)) {
                continue;
            }

            $filterArrayInner = array();
            foreach ($findArrayInner as $findValue) {
                $operator = $findValue[1];
                $filterArrayInner[] = $query->$operator($findValue[0], $findValue[2]);
            }
            $filterArray[] = $query->logicalOr($filterArrayInner);

        }

        $query->matching($query->logicalAnd($filterArray));

        $result = $query->execute();
        return $result;
    }

    public function setLimit($limit)
    {
        if ((int) $limit > 0) {
            $this->limit = (int) $limit;
        }

        return $this;
    }

    public function setOffset($offset)
    {
        if ((int) $offset > 0) {
            $this->offset = (int) $offset;
        }

        return $this;
    }

    public function setOrderings($orderings)
    {
        $this->orderings = $orderings;

        return $this;
    }

    public function setRespectEnableFields($respectEnableFields)
    {
        $this->respectEnableFields = $respectEnableFields;

        return $this;
    }

    public function setRespectStoragePage($respectStoragePage)
    {
        $this->respectStoragePage = $respectStoragePage;

        return $this;
    }

}
