<?php
namespace MED\Medgooglemaps\Controller;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use FluidTYPO3\Vhs\Asset;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

/**
 * MapsController
 */
class MapsController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * mapsRepository
     *
     * @var \MED\Medgooglemaps\Domain\Repository\MapsRepository
     * @inject
     */
    protected $mapsRepository = null;

    /**
     * markerRepository
     *
     * @var \MED\Medgooglemaps\Domain\Repository\MarkerRepository
     * @inject
     */
    protected $markerRepository = null;

    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        // Add jQuery?
        if ($this->settings['addJquery']) {
            if ($this->settings['addToFooter']) {
                $GLOBALS['TSFE']->additionalFooterData['medgooglemaps_jquery'] = '<script type="text/javascript" src="' . ExtensionManagementUtility::siteRelPath('medgooglemaps') . 'Resources/Public/Js/jquery-1.11.3.min.js"></script>';
            } else {
                $GLOBALS['TSFE']->additionalHeaderData['medgooglemaps_jquery'] = '<script type="text/javascript" src="' . ExtensionManagementUtility::siteRelPath('medgooglemaps') . 'Resources/Public/Js/jquery-1.11.3.min.js"></script>';
            }
        }

        $maps = $this->mapsRepository->findAll();

        // Add JS files
        $GLOBALS['TSFE']->additionalHeaderData['medgooglemaps_api'] = '<script type="text/javascript" src="' . $this->settings['apiUrl'] . '?key=' . $this->settings['apiKey'] . '"></script>';

        // Controls
        $controls = $this->settings['controls'];

        if ($controls) {
            $controlsArr = explode(",", $controls);
            $controlsConfig = ',';
            foreach ($controlsArr as $c) {
                if ($c == 'scaleControl' || $c == 'disableDoubleClickZoom') {
                    $controlsConfig .= '
                        ' . $c . ': true,
                    ';
                } else {
                    $controlsConfig .= '
                        ' . $c . ': false,
                    ';
                }
            }
            $controlsConfig = substr(trim($controlsConfig), 0, -1);
        }

        // Marker new
        $markers = explode(",", $this->settings['marker']);

        $markers = $this->markerRepository->setRespectStoragePage(false)->findByFilter(array(
            array(
                array(
                    'uid',
                    'in',
                    $markers,
                ),
            ),
        ));

        $cObj = $this->configurationManager->getContentObject();
        $uid = $cObj->data['uid'];

        // Template vars
        $this->view->assign('settings', $this->settings);
        $this->view->assign('uid', $uid);

        /*
        JS
         */

        // Lat + Lng
        $latitude = $this->settings['latitude'];
        $longitude = $this->settings['longitude'];

        if ($latitude) {
            $latitude = $latitude;
        } else {
            $latitude = 0;
        }

        if ($longitude) {
            $longitude = $longitude;
        } else {
            $longitude = 0;
        }

        // Zoom
        $zoom = $this->settings['zoom'];

        // Style
        $style = $this->settings['style'];
        if ($style) {
            $theme = $this->getPartialView('Theme-' . $this->settings['style'], array(), 'Themes/');

            $styleOutput = 'styles: ' . $theme->render() . ',';
        }

        // Map type
        $mapType = $this->settings['mapType'];
        if ($mapType) {
            $mapTypeOutput = $mapType;
        } else {
            $mapTypeOutput = 'MapTypeId.ROADMAP';
        }

        // Navigation
        $navigationArray = array();
        if ($markers) {
            $c = 1;

            foreach ($markers as $marker) {
                if ($marker->getNavigation()) {
                    // Config for fluid standalone view for markers
                    $confArr = array(
                        'settings' => array(
                            'googleMapsSubmitClasses' => $this->settings['googleMapsSubmitClasses'],
                        ),
                        'marker' => $marker,
                    );

                    $navigation = $this->getPartialView('Navigation', $confArr);
                    $navigationArray[$c] = json_encode($navigation->render());
                }
            }
        }

        if (intval($this->settings['deactivateScrollWheel']) != 0) {
            $scrollWheel = 'false';
        } else {
            $scrollWheel = 'true';
        }

        if (intval($this->settings['deactivateDragging']) != 0) {
            $draggable = 'false';
        } else {
            $draggable = 'true';
        }

        Asset::createFromSettings(array(
            'name' => 'medgooglemapsCustom' . $uid,
            'path' => 'EXT:medgooglemaps/Resources/Public/Js/medgooglemaps.js',
            'dependencies' => 'medgooglemaps',
            'fluid' => true,
            'variables' => array(
                'uid' => $uid,
                'latitude' => $latitude,
                'longitude' => $longitude,
                'scrollWheel' => $scrollWheel,
                'draggable' => $draggable,
                'zoom' => $zoom,
                'styleOutput' => $styleOutput,
                'mapTypeOutput' => $mapTypeOutput,
                'controlsConfig' => $controlsConfig,
                'markers' => $markers,
                'centerOnResize' => $this->settings['centerOnResize'],
                'navigationArray' => $navigationArray,
                // 'customMarkerPath' => $this->settings['customMarkerPath'],
                'autocalcmapcenter' => $this->settings['autocalcmapcenter'],
            ),
        ));
    }

    protected function getPartialView($templateName, array $variables = array(), $prefix = '')
    {
        $partialView = new \TYPO3\CMS\Fluid\View\StandaloneView();
        $partialView->setFormat('html');
        $extbaseFrameworkConfiguration = $this->configurationManager->getConfiguration(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK);
        $templateRootPath = \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName($extbaseFrameworkConfiguration['view']['partialRootPaths'][0]);
        $templatePathAndFilename = $templateRootPath . $prefix . $templateName . '.html';
        $partialView->setTemplatePathAndFilename($templatePathAndFilename);
        $partialView->assignMultiple($variables);
        $extensionName = $this->request->getControllerExtensionName();
        $partialView->getRequest()->setControllerExtensionName($extensionName);

        return $partialView;
    }

}