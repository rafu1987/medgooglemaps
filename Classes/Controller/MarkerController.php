<?php
namespace MED\Medgooglemaps\Controller;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

/**
 * MarkerController
 */
class MarkerController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * markerRepository
     *
     * @var \MED\Medgooglemaps\Domain\Repository\MarkerRepository
     * @inject
     */
    protected $markerRepository = null;

    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        $markers = $this->markerRepository->findAll();
        $this->view->assign('markers', $markers);
    }

}
