<?php
namespace MED\Medgooglemaps\Utility;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

class Geocode
{
    public function geocode($PA, $fobj)
    {
        $uid = $PA['row']['uid'];

        $extConf = $this->getExtensionConfig();

        if (\TYPO3\CMS\Core\Utility\VersionNumberUtility::convertVersionNumberToInteger(TYPO3_version) >= '7006000') {
            $content .= '
                <style type="text/css">
                    input.medgooglemaps_geocode_input {
                        font-weight: bold;
                    }
                </style>

                <script type="text/javascript">
                        TYPO3.jQuery(document).ready(function() {
                            TYPO3.jQuery("a.medgooglemaps_geocode").click(function(e) {
                                e.preventDefault();

                                TYPO3.jQuery.ajax({
                                  url: "../' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::siteRelPath('medgooglemaps') . 'Resources/Public/Php/geocode.php",
                                  type: "POST",
                                  data: {
                                    apiKey: "' . $extConf['apiKey'] . '",
                                    address: TYPO3.jQuery("input[data-formengine-input-name=\'data[tt_content][' . $uid . '][pi_flexform][data][sDEF][lDEF][settings.address][vDEF]\']").val(),
                                    zip: TYPO3.jQuery("input[data-formengine-input-name=\'data[tt_content][' . $uid . '][pi_flexform][data][sDEF][lDEF][settings.zip][vDEF]\']").val(),
                                    city: TYPO3.jQuery("input[data-formengine-input-name=\'data[tt_content][' . $uid . '][pi_flexform][data][sDEF][lDEF][settings.city][vDEF]\']").val(),
                                    country: TYPO3.jQuery("input[data-formengine-input-name=\'data[tt_content][' . $uid . '][pi_flexform][data][sDEF][lDEF][settings.country][vDEF]\']").val()
                                  },
                                  success: function(data) {
                                    var obj = TYPO3.jQuery.parseJSON(data);

                                    // Latitude
                                    TYPO3.jQuery("input[data-formengine-input-name=\'data[tt_content][' . $uid . '][pi_flexform][data][sDEF][lDEF][settings.latitude][vDEF]\']").val(obj.lat);
                                    TYPO3.jQuery("input[name=\'data[tt_content][' . $uid . '][pi_flexform][data][sDEF][lDEF][settings.latitude][vDEF]\']").val(obj.lat);

                                    // Longitude
                                    TYPO3.jQuery("input[data-formengine-input-name=\'data[tt_content][' . $uid . '][pi_flexform][data][sDEF][lDEF][settings.longitude][vDEF]\']").val(obj.lng);
                                    TYPO3.jQuery("input[name=\'data[tt_content][' . $uid . '][pi_flexform][data][sDEF][lDEF][settings.longitude][vDEF]\']").val(obj.lng);
                                  }
                                });
                            });
                        });
                </script>
            ';
        } else {
            $content .= '
                <style type="text/css">
                    input.medgooglemaps_geocode_input {
                        font-weight: bold;
                    }
                </style>

                <script type="text/javascript">
                        TYPO3.jQuery(document).ready(function() {
                            TYPO3.jQuery("a.medgooglemaps_geocode").click(function(e) {
                                e.preventDefault();

                                TYPO3.jQuery.ajax({
                                  url: "../' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::siteRelPath('medgooglemaps') . 'Resources/Public/Php/geocode.php",
                                  type: "POST",
                                  data: {
                                    apiKey: "' . $extConf['apiKey'] . '",
                                    address: TYPO3.jQuery("input[name=\'data[tt_content][' . $uid . '][pi_flexform][data][sDEF][lDEF][settings.address][vDEF]_hr\']").val(),
                                    zip: TYPO3.jQuery("input[name=\'data[tt_content][' . $uid . '][pi_flexform][data][sDEF][lDEF][settings.zip][vDEF]_hr\']").val(),
                                    city: TYPO3.jQuery("input[name=\'data[tt_content][' . $uid . '][pi_flexform][data][sDEF][lDEF][settings.city][vDEF]_hr\']").val(),
                                    country: TYPO3.jQuery("input[name=\'data[tt_content][' . $uid . '][pi_flexform][data][sDEF][lDEF][settings.country][vDEF]_hr\']").val()
                                  },
                                  success: function(data) {
                                    var obj = TYPO3.jQuery.parseJSON(data);

                                    // Latitude
                                    TYPO3.jQuery("input[name=\'data[tt_content][' . $uid . '][pi_flexform][data][sDEF][lDEF][settings.latitude][vDEF]_hr\']").val(obj.lat);
                                    TYPO3.jQuery("input[name=\'data[tt_content][' . $uid . '][pi_flexform][data][sDEF][lDEF][settings.latitude][vDEF]\']").val(obj.lat);

                                    // Longitude
                                    TYPO3.jQuery("input[name=\'data[tt_content][' . $uid . '][pi_flexform][data][sDEF][lDEF][settings.longitude][vDEF]_hr\']").val(obj.lng);
                                    TYPO3.jQuery("input[name=\'data[tt_content][' . $uid . '][pi_flexform][data][sDEF][lDEF][settings.longitude][vDEF]\']").val(obj.lng);
                                  }
                                });
                            });
                        });
                </script>
            ';
        }

        $content .= '
            <div class="typo3-newRecordLink">
                <a href="javascript:void(0);" class="btn btn-default medgooglemaps_geocode"><img src="/typo3conf/ext/medgooglemaps/Resources/Public/Icons/geocode.svg" width="16" height="16" alt="">' . $GLOBALS['LANG']->sL('LLL:EXT:medgooglemaps/Resources/Private/Language/locallang_be.xlf:geocode_address') . '</a>
            </div>
        ';

        return $content;
    }

    public function geocodeSectionIrre($PA, $fobj)
    {
        $uid = $PA['row']['uid'];

        $extConf = $this->getExtensionConfig();

        if (\TYPO3\CMS\Core\Utility\VersionNumberUtility::convertVersionNumberToInteger(TYPO3_version) >= '7006000') {
            $content .= '
                <style type="text/css">
                    .typo3-TCEforms .t3-form-field-container-inline .wrapperTable, .typo3-TCEforms .t3-form-field-container-inline .wrapperTable1, .typo3-TCEforms .t3-form-field-container-inline .wrapperTable2, .typo3-TCEforms .t3-form-field-container-inline .wrapperTable3, .typo3-TCEforms .t3-form-field-container-inline .wrapperTable4, .typo3-TCEforms .t3-form-field-container-inline .wrapperTable5 {
                        border-bottom: 1px solid #c0c0c0;
                    }
                    input.medgooglemaps_geocode_input {
                        font-weight: bold;
                    }
                </style>

                <script type="text/javascript">
                        TYPO3.jQuery(document).ready(function() {
                            TYPO3.jQuery("a.medgooglemaps_geocode_irre_' . $uid . '").click(function(e) {
                                e.preventDefault();

                                TYPO3.jQuery.ajax({
                                  url: "../' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::siteRelPath('medgooglemaps') . 'Resources/Public/Php/geocode.php",
                                  type: "POST",
                                  data: {
                                    apiKey: "' . $extConf['apiKey'] . '",
                                    address: TYPO3.jQuery("input[data-formengine-input-name=\'data[tx_medgooglemaps_domain_model_marker][' . $uid . '][address]\']").val(),
                                    zip: TYPO3.jQuery("input[data-formengine-input-name=\'data[tx_medgooglemaps_domain_model_marker][' . $uid . '][zip]\']").val(),
                                    city: TYPO3.jQuery("input[data-formengine-input-name=\'data[tx_medgooglemaps_domain_model_marker][' . $uid . '][city]\']").val(),
                                    country: TYPO3.jQuery("input[data-formengine-input-name=\'data[tx_medgooglemaps_domain_model_marker][' . $uid . '][country]\']").val()
                                  },
                                  success: function(data) {
                                    var obj = TYPO3.jQuery.parseJSON(data);

                                    // Latitude
                                    TYPO3.jQuery("input[data-formengine-input-name=\'data[tx_medgooglemaps_domain_model_marker][' . $uid . '][latitude]\']").val(obj.lat);
                                    TYPO3.jQuery("input[name=\'data[tx_medgooglemaps_domain_model_marker][' . $uid . '][latitude]\']").val(obj.lat);

                                    // Longitude
                                    TYPO3.jQuery("input[data-formengine-input-name=\'data[tx_medgooglemaps_domain_model_marker][' . $uid . '][longitude]\']").val(obj.lng);
                                    TYPO3.jQuery("input[name=\'data[tx_medgooglemaps_domain_model_marker][' . $uid . '][longitude]\']").val(obj.lng);
                                  }
                                });
                            });
                        });
                </script>
            ';
        } else {
            $content .= '
                <style type="text/css">
                    .typo3-TCEforms .t3-form-field-container-inline .wrapperTable, .typo3-TCEforms .t3-form-field-container-inline .wrapperTable1, .typo3-TCEforms .t3-form-field-container-inline .wrapperTable2, .typo3-TCEforms .t3-form-field-container-inline .wrapperTable3, .typo3-TCEforms .t3-form-field-container-inline .wrapperTable4, .typo3-TCEforms .t3-form-field-container-inline .wrapperTable5 {
                        border-bottom: 1px solid #c0c0c0;
                    }
                    input.medgooglemaps_geocode_input {
                        font-weight: bold;
                    }
                </style>

                <script type="text/javascript">
                        TYPO3.jQuery(document).ready(function() {
                            TYPO3.jQuery("a.medgooglemaps_geocode_irre_' . $uid . '").click(function(e) {
                                e.preventDefault();

                                TYPO3.jQuery.ajax({
                                  url: "../' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::siteRelPath('medgooglemaps') . 'Resources/Public/Php/geocode.php",
                                  type: "POST",
                                  data: {
                                    apiKey: "' . $extConf['apiKey'] . '",
                                    address: TYPO3.jQuery("input[name=\'data[tx_medgooglemaps_domain_model_marker][' . $uid . '][address]_hr\']").val(),
                                    zip: TYPO3.jQuery("input[name=\'data[tx_medgooglemaps_domain_model_marker][' . $uid . '][zip]_hr\']").val(),
                                    city: TYPO3.jQuery("input[name=\'data[tx_medgooglemaps_domain_model_marker][' . $uid . '][city]_hr\']").val(),
                                    country: TYPO3.jQuery("input[name=\'data[tx_medgooglemaps_domain_model_marker][' . $uid . '][country]_hr\']").val()
                                  },
                                  success: function(data) {
                                    var obj = TYPO3.jQuery.parseJSON(data);

                                    // Latitude
                                    TYPO3.jQuery("input[name=\'data[tx_medgooglemaps_domain_model_marker][' . $uid . '][latitude]_hr\']").val(obj.lat);
                                    TYPO3.jQuery("input[name=\'data[tx_medgooglemaps_domain_model_marker][' . $uid . '][latitude]\']").val(obj.lat);

                                    // Longitude
                                    TYPO3.jQuery("input[name=\'data[tx_medgooglemaps_domain_model_marker][' . $uid . '][longitude]_hr\']").val(obj.lng);
                                    TYPO3.jQuery("input[name=\'data[tx_medgooglemaps_domain_model_marker][' . $uid . '][longitude]\']").val(obj.lng);
                                  }
                                });
                            });
                        });
                </script>
            ';
        }

        $content .= '
            <div class="typo3-newRecordLink">
                <a href="javascript:void(0);" class="btn btn-default medgooglemaps_geocode_irre_' . $uid . '"><img src="/typo3conf/ext/medgooglemaps/Resources/Public/Icons/geocode.svg" width="16" height="16" alt="">' . $GLOBALS['LANG']->sL('LLL:EXT:medgooglemaps/Resources/Private/Language/locallang_be.xlf:geocode_address') . '</a>
            </div>
        ';

        return $content;
    }

    /**
     * Get extension config
     *
     * @return void
     */
    private function getExtensionConfig()
    {
        $extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['medgooglemaps']);

        return $extConf;
    }
}
