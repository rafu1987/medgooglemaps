<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "medgooglemaps".
 *
 * Auto generated 30-05-2019 19:39
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = [
    'title' => 'Google Maps',
    'description' => 'Google Maps',
    'category' => 'plugin',
    'version' => '1.0.6',
    'state' => 'beta',
    'uploadfolder' => false,
    'createDirs' => '',
    'clearCacheOnLoad' => false,
    'author' => 'Raphael Zschorsch',
    'author_email' => 'rafu1987@gmail.com',
    'author_company' => null,
    'constraints' => [
        'depends' => [
            'extbase' => '8.7.0-8.7.99',
            'fluid' => '8.7.0-8.7.99',
            'typo3' => '8.7.0-8.7.99',
            'php' => '7.0.0-7.2.99',
            'vhs' => '',
        ],
        'conflicts' => [
        ],
        'suggests' => [
        ],
    ],
    'autoload' => [
        'psr-4' => [
            'MED\\Medgooglemaps\\' => 'Classes',
        ],
    ],
    'clearcacheonload' => false,
];